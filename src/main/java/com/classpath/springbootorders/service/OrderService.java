package com.classpath.springbootorders.service;

import com.classpath.springbootorders.exception.OrderNotFoundException;
import com.classpath.springbootorders.model.Order;
import com.classpath.springbootorders.repository.OrderDAO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class OrderService {

    private OrderDAO orderDAO;

    public Order saveOrder(Order order){
        log.trace("This is a trace log in the save order method");
        log.debug("This is a debug log in the save order method");
        log.info("This is a info log in the save order method");
        log.warn("This is a warn log in the save order method");
        log.error("This is a error log in the save order method");
        return this.orderDAO.save(order);
    }

    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderDAO.findAll());
    }

    public Order findById(long orderId) throws OrderNotFoundException {
        return this.orderDAO
                        .findById(orderId)
                        .orElseThrow(() -> new OrderNotFoundException("Invalid Order Id"));
    }

    public void deleteOrder(long orderId){
        this.orderDAO.deleteById(orderId);
    }
}