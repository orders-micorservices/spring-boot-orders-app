package com.classpath.springbootorders.controller;

import com.classpath.springbootorders.exception.OrderNotFoundException;
import com.classpath.springbootorders.model.Order;
import com.classpath.springbootorders.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
@Slf4j
public class OrderController {

    private OrderService orderService;

    @PostMapping
    @ResponseStatus(CREATED)
    public Order createOrder(@Valid @RequestBody Order order){
        log.info("Came inside the create order method {}", order);
        return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Set<Order> fetchAllOrders(){
        log.info("Came inside the fetchAll Orders method");
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    public Order fetchById(@PathVariable("id") long orderId) throws OrderNotFoundException {
        return this.orderService.findById(orderId);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") long orderId){
        this.orderService.deleteOrder(orderId);
    }
}