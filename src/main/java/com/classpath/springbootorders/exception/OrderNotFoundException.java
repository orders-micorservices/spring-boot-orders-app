package com.classpath.springbootorders.exception;

public class OrderNotFoundException extends Exception {

    public OrderNotFoundException(String message){
        super(message);
    }

    public String getMessage(){
        return super.getMessage();
    }
}