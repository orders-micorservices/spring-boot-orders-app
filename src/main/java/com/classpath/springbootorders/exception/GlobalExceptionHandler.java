package com.classpath.springbootorders.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<String> handleInvalidOrderException(Exception ex){
        return ResponseEntity.badRequest().body("{\"status\":" +"ex.getMessage()+}");
    }
}