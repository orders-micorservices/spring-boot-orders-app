package com.classpath.springbootorders.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import static javax.persistence.GenerationType.AUTO;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @Min(value = 25000, message = "Minimum order price should be 25K")
    private double price;

    @NotNull(message = "orderDate cannot be null")
    private LocalDate orderDate;

    @NotEmpty(message = "customer name cannot be blank")
    private String customerName;
}

